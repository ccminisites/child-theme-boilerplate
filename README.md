# Child Theme Boilerplate

This is the boiler plate for a child theme that will inherent from the main theme and get your started. 

This is going to be where you make theme changes in each project.

**This script will download the child theme boilerplate and put it into your project**

**it will overwrite wp-content/theme/project-child**

**After you have done this once in the project, you should never do it again!**

After you have done this your project now has a project specific child theme that should be committed in the repo of the project!

Once your child theme is being used in the project, be sure to change the gitlab variables to use this theme.

## RUN THIS ONCE IN WP-CONTENT/THEMES! ##

It will overwrite **wp-content/theme/project-child**

```bash <(wget -qO- https://gitlab.com/ccminisites/child-theme-boilerplate/-/raw/master/download.sh)```

!!! You need to add to the project .gitignore !!!

```
!wp-content/themes/project-child
!wp-content/themes/project-child/*
!wp-content/themes/project-child/*/
!wp-content/themes/project-child/*/*
!wp-content/themes/project-child/*/*/
!wp-content/themes/project-child/*/*/*
```
