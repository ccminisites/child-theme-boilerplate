<?php
/**
 * HI!
 *
 * Does you child theme need to disable stuff from the parent theme and maybe enable lazy blocks of it own?
 *
 * See the projects eiaweb for that
 */

/**
 * Hello, it's me again!
 *
 * 
 */

/**
 * Register the scripts and styles for the parent and child
 * Child style.css will start overriding parent style.css.
 */
function project_child_add_scripts_and_styles()
{
    wp_enqueue_style('style-parent', get_template_directory_uri() . '/style.css', ['bootstrap']);
    wp_enqueue_style('style-child', get_stylesheet_directory_uri() . '/style.css', ['bootstrap','style-parent']);
    wp_enqueue_script('project-child', get_stylesheet_directory_uri() . '/js/project-child.js', [], false, true);
}

add_action('wp_enqueue_scripts', 'project_child_add_scripts_and_styles');
